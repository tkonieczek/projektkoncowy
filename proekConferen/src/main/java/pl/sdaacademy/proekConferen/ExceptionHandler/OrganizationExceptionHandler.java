package pl.sdaacademy.proekConferen.ExceptionHandler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import pl.sdaacademy.proekConferen.exception.OrganisationAlreadyExistException;

import javax.persistence.EntityNotFoundException;


@SuppressWarnings({"unchecked", "rawtypes"})
@ControllerAdvice
public class OrganizationExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler({OrganisationAlreadyExistException.class, EntityNotFoundException.class})
    protected ResponseEntity<Object> handleEntityNotFoundEx(OrganisationAlreadyExistException ex, WebRequest request) {
        OrganisationAlreadyExistException apiError = new OrganisationAlreadyExistException("Entity Not Found Exception", ex.getMessage());
        return new ResponseEntity<>(apiError, HttpStatus.NOT_FOUND);
    }


}