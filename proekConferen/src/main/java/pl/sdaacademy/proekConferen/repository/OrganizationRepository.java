package pl.sdaacademy.proekConferen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sdaacademy.proekConferen.model.OrganizationEntity;

import java.util.Optional;


@Repository
public interface OrganizationRepository extends JpaRepository<OrganizationEntity, Integer> {

    Optional<OrganizationEntity> findByName(String name);
}
