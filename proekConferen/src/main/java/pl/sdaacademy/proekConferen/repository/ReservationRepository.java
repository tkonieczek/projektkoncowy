package pl.sdaacademy.proekConferen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sdaacademy.proekConferen.model.ReservationEntity;

import java.util.Optional;

@Repository
public interface ReservationRepository extends JpaRepository<ReservationEntity, Integer> {
    Optional<ReservationEntity> findByName(String name);
/*
    @Query("SELECT reservation FROM ReservationEntity reservation WHERE reservation.reservationOrganization = :name")
    Optional<ReservationEntity> findReservationByName(String name);
/*
    @Query("SELECT reservation FROM ReservationEntity reservation INNER JOIN PersonEntity person WHERE person.personName = :name")
    List<ReservationEntity> findAllFromUser(String name);

    @Query("SELECT reservation FROM ReservationEntity reservation WHERE reservation.date BETWEEN :firstDate AND :secondDate")
    List<ReservationEntity> findAllBetweenDates(LocalDate firstDate, LocalDate secondDate);

    @Query("SELECT reservation FROM ReservationEntity reservation INNER JOIN PersonEntity person WHERE person.personName = :username AND reservation.date = :date AND reservation.time BETWEEN :firstTime AND :secondTime")
    List<ReservationEntity> findAllBetweenTimesInDateFromUser(String username, LocalDate date, LocalTime firstTime, LocalTime secondTime);


 */
}

