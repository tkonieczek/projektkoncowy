package pl.sdaacademy.proekConferen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.sdaacademy.proekConferen.model.ConferenceRoomEntity;

import java.util.Optional;

@Repository
public interface ConferenceRoomRepository extends JpaRepository<ConferenceRoomEntity, Integer> {
    Optional<ConferenceRoomEntity> findByName(String name);

    // po dostępności
 //   @Query("SELECT conferenceroom FROM ConferenceRoomEntity conferenceroom WHERE conferenceroom.availability = :availability")
    Optional<ConferenceRoomEntity> findByAvailability(Boolean Availability);

    //private String identifier;
    //private String levelStatus;
    //private Boolean availability;
    //private Integer numerOfSeats;

}
