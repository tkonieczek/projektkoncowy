package pl.sdaacademy.proekConferen.services;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sdaacademy.proekConferen.exception.RecordNotFoundException;
import pl.sdaacademy.proekConferen.model.ReservationEntity;
import pl.sdaacademy.proekConferen.repository.ReservationRepository;

@Service
public class ReservationService {

    private final ReservationRepository reservationRepository;

    @Autowired
    ReservationService(ReservationRepository reservationRepository) {
        this.reservationRepository = reservationRepository;
    }


    public ReservationEntity getReservationByName(@NonNull String name) {
        return reservationRepository.findByName(name)
                .orElseThrow(() -> new RecordNotFoundException("Organization Not Found:  " + name));
    }

}
/*
    private final ReservationRepository reservationRepository;
    private final OrganizationRepository organizationRepository;
    @Autowired
    public ReservationService(ReservationRepository reservationRepository, OrganizationRepository organizationRepository) {
        this.reservationRepository = reservationRepository;
        this.organizationRepository = organizationRepository;
    }





    public ReservationEntity getReservationByName(String reservationName) {
        return reservationRepository.findReservationByName(reservationName)
                .orElseThrow(() -> new NoSuchElementException("Reservation not found:" + reservationName));
    }

}
/*

    void addReservation(ReservationEntity reservation) {
        reservationRepository.findById(reservation.getId()).ifPresent(r -> {
            throw new RuntimeException("Element Already Exists");
        });
    }
}
/*
List<ReservationEntity> getAllReservationsFromUser(String username) {
        return reservationRepository.findAllFromUser(username);
    }
    void deleteReservation(ReservationEntity reservation) {
        ReservationEntity result = reservationRepository.findById(reservation.getId())
                .orElseThrow(() -> new NoSuchElementException("Reservation not found: " + reservation.getReservationOrganization()));
        reservationRepository.delete(reservation);
    }

    void updateReservationName(ReservationEntity reservation, String name) {
        reservationRepository.findById(reservation.getId()).ifPresentOrElse(
                result -> {
                    reservation.setReservationOrganization(name);
                    reservationRepository.save(reservation);
                },
                () -> {
                    throw new NoSuchElementException("Reservation not found: " + reservation.getReservationOrganization());
                }
        );
    }


*/

