package pl.sdaacademy.proekConferen.services;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sdaacademy.proekConferen.exception.RecordAlreadyExistsException;
import pl.sdaacademy.proekConferen.exception.RecordNotFoundException;
import pl.sdaacademy.proekConferen.model.ConferenceRoomEntity;
import pl.sdaacademy.proekConferen.model.OrganizationEntity;
import pl.sdaacademy.proekConferen.repository.ConferenceRoomRepository;

import java.util.Collections;
import java.util.List;

@Service
public class ConferenceRoomService {
    private final ConferenceRoomRepository conferenceRoomRepository;

    @Autowired
    ConferenceRoomService(ConferenceRoomRepository conferenceRoomRepository) {
        this.conferenceRoomRepository = conferenceRoomRepository;
    }

    public ConferenceRoomEntity getConferenceRoomByName(@NonNull String name) {
        return conferenceRoomRepository.findByName(name)
                .orElseThrow(() -> new RecordNotFoundException("Organization Not Found:  " + name));
    }

    public void addConferenceRoom(@NonNull ConferenceRoomEntity conferenceRoomEntity) {
        conferenceRoomRepository.findByName(conferenceRoomEntity.getName()).ifPresent(org -> {
            throw new RecordAlreadyExistsException("Organization already exists: " + conferenceRoomEntity.getName());
        });
        conferenceRoomRepository.save(conferenceRoomEntity);
    }

    public void removeConferenceRoomByName(@NonNull String name) {
        ConferenceRoomEntity conferenceRoomEntity = conferenceRoomRepository.findByName(name)
                .orElseThrow(() -> new RecordNotFoundException("Can't find organization with name: " + name));
        conferenceRoomRepository.delete(conferenceRoomEntity);
    }

    public void updateConferenceRoomn(@NonNull String oldName, @NonNull String newName) {
        conferenceRoomRepository.findByName(oldName).ifPresentOrElse(conf -> {
            conf.setName(newName);
            conferenceRoomRepository.save(conf);
        }, () -> {
            throw new RecordNotFoundException("Organization Not Found");
        });
    }




    public List<ConferenceRoomEntity> getConferenceRoomByAvailability(@NonNull Boolean availability) {
            return conferenceRoomRepository.findByAvailability(availability)
                    .map(Collections::singletonList)
                    .orElse(Collections.emptyList());
        }
        //return conferenceRoomRepository.findAll();

}
