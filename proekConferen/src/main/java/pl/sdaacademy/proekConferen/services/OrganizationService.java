package pl.sdaacademy.proekConferen.services;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sdaacademy.proekConferen.exception.RecordAlreadyExistsException;
import pl.sdaacademy.proekConferen.exception.RecordNotFoundException;
import pl.sdaacademy.proekConferen.model.OrganizationEntity;
import pl.sdaacademy.proekConferen.repository.OrganizationRepository;

import java.util.Collections;
import java.util.List;

@Service
public class OrganizationService {

    private final OrganizationRepository organizationRepository;

    @Autowired
    OrganizationService(OrganizationRepository organizationRepository) {
        this.organizationRepository = organizationRepository;
    }

    public void addOrganization(@NonNull OrganizationEntity organizationEntity) {
        organizationRepository.findByName(organizationEntity.getName()).ifPresent(org -> {
            throw new RecordAlreadyExistsException("Organization already exists: " + organizationEntity.getName());
        });
        organizationRepository.save(organizationEntity);
    }

    public OrganizationEntity getOrganizationByName(@NonNull String name) {
        return organizationRepository.findByName(name)
                .orElseThrow(() -> new RecordNotFoundException("Organization Not Found:  " + name));
    }

    public void removeOrganizationByName(@NonNull String name) {
        OrganizationEntity organizationEntity = organizationRepository.findByName(name)
                .orElseThrow(() -> new RecordNotFoundException("Can't find organization with name: " + name));
        organizationRepository.delete(organizationEntity);

    }

    public void updateOrganization(@NonNull String oldName, @NonNull String newName) {
        organizationRepository.findByName(oldName).ifPresentOrElse(org -> {
            org.setName(newName);
            organizationRepository.save(org);
        }, () -> {
            throw new RecordNotFoundException("Organization Not Found");
        });
    }

    public List<OrganizationEntity> getOrganizations(String name) {
        if (name != null) {
            return organizationRepository.findByName(name)
                    .map(Collections::singletonList)
                    .orElse(Collections.emptyList());
        }
        return organizationRepository.findAll();
    }
}
