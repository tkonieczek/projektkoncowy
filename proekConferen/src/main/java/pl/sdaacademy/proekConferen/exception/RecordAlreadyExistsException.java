package pl.sdaacademy.proekConferen.exception;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RecordAlreadyExistsException extends RuntimeException {

    private String message;


    public RecordAlreadyExistsException(String message) {
        this.message = message;

    }
}