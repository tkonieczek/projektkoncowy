package pl.sdaacademy.proekConferen.exception;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data

@NoArgsConstructor
public class RecordNotFoundException extends RuntimeException {

    private String message;


    public RecordNotFoundException(String message) {
        this.message = message;

    }
}