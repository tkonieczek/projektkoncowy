package pl.sdaacademy.proekConferen.exception;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrganisationAlreadyExistException extends RuntimeException {

    private String message;
    private String debugMessage;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<String> errors;


    public OrganisationAlreadyExistException(String message, String debugMessage) {
        this.message = message;
        this.debugMessage = debugMessage;
    }
}
