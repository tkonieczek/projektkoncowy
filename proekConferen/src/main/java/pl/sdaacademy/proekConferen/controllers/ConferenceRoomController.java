package pl.sdaacademy.proekConferen.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sdaacademy.proekConferen.model.ConferenceRoomEntity;
import pl.sdaacademy.proekConferen.model.OrganizationEntity;
import pl.sdaacademy.proekConferen.services.ConferenceRoomService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/conferencerooms")
public class ConferenceRoomController {
    private final ConferenceRoomService conferenceRoomService;

    @Autowired
    ConferenceRoomController(ConferenceRoomService conferenceRoomService) {
        this.conferenceRoomService = conferenceRoomService;
    }

    @GetMapping("/{name}")
    ResponseEntity<ConferenceRoomEntity> getConferenceRoomByName(@PathVariable String name) {
        ConferenceRoomEntity result = conferenceRoomService.getConferenceRoomByName(name);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @DeleteMapping("/{name}")
    ResponseEntity<String> removeConferenceRoom(@PathVariable("name") String name) {
        conferenceRoomService.removeConferenceRoomByName(name);
        return new ResponseEntity<>("Removed the organization: " + name, HttpStatus.OK);
    }

    @PostMapping
    ResponseEntity<String> addConferenceRoom(@Valid @RequestBody ConferenceRoomEntity conferenceRoomEntity) {
        conferenceRoomService.addConferenceRoom(conferenceRoomEntity);
        return new ResponseEntity<>("OK" + conferenceRoomEntity.getName(), HttpStatus.OK);
    }

    @PutMapping
    ResponseEntity<String> updateConferenceRoom(@RequestParam("old_name_org") String oldNameOrg, @RequestParam("new_name_org") String newNameOrg) {
        conferenceRoomService.updateConferenceRoomn(oldNameOrg, newNameOrg);
        return new ResponseEntity<>(String.format("Updated organization: %s to %s", oldNameOrg, newNameOrg), HttpStatus.OK);
    }
/*
    //po dostępności
    @GetMapping("/{availability}")
    ResponseEntity<ConferenceRoomEntity> getConferenceRoomByAvailability(@PathVariable Boolean availability) {
        List<ConferenceRoomEntity> result = conferenceRoomService.getConferenceRoomByAvailability(availability);
        return result;
        //return new ResponseEntity<>(result, HttpStatus.OK);
    }


 */
}
