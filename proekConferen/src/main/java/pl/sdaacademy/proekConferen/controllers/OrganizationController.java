package pl.sdaacademy.proekConferen.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import pl.sdaacademy.proekConferen.model.OrganizationEntity;
import pl.sdaacademy.proekConferen.services.OrganizationService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/organizations")
class OrganizationController {

    private final OrganizationService organizationService;

    @Autowired
    OrganizationController(OrganizationService organizationService) {
        this.organizationService = organizationService;
    }

    @GetMapping
    List<OrganizationEntity> getAllOrganizations(
            @RequestParam(required = false) String name
    ) {
        return organizationService.getOrganizations(name);
    }


    @GetMapping("/{name}")
    ResponseEntity<OrganizationEntity> getOrganizationByName(@PathVariable String name) {
        OrganizationEntity result = organizationService.getOrganizationByName(name);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @DeleteMapping("/{name}")
    ResponseEntity<String> removeOrganization(@PathVariable("name") String name) {
        organizationService.removeOrganizationByName(name);
        return new ResponseEntity<>("Removed the organization: " + name, HttpStatus.OK);
    }


    @PostMapping
    ResponseEntity<String> addOrganization(@Valid @RequestBody OrganizationEntity organizationEntity) {
        organizationService.addOrganization(organizationEntity);
        return new ResponseEntity<>("OK" + organizationEntity.getName(), HttpStatus.OK);
    }

    @PutMapping
    ResponseEntity<String> updateOrganization(@RequestParam("old_name_org") String oldNameOrg, @RequestParam("_name_org") String newNameOrg) {
        organizationService.updateOrganization(oldNameOrg, newNameOrg);
        return new ResponseEntity<>(String.format("Updated organization: %s to %s", oldNameOrg, newNameOrg), HttpStatus.OK);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    ResponseEntity<Object> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    ResponseEntity<Object> handleIllegalArgumentException(IllegalArgumentException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NoSuchElementException.class)
    ResponseEntity<Object> handleNoSuchElementException(NoSuchElementException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
    }
}
