package pl.sdaacademy.proekConferen.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.sdaacademy.proekConferen.model.ReservationEntity;
import pl.sdaacademy.proekConferen.services.ReservationService;

@RestController
@RequestMapping("/reservations")
public class ReservationController {
    private final ReservationService reservationService;

    @Autowired
    ReservationController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @GetMapping("/{name}")
    ResponseEntity<ReservationEntity> getReservationByName(@PathVariable String name) {
        ReservationEntity result = reservationService.getReservationByName(name);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


}
