package pl.sdaacademy.proekConferen.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Data
@Table(name = "organization")
public class OrganizationEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "name", nullable = false)
    @Size(min = 2, max = 20)
    @NotBlank
    private String name;

    @OneToMany(mappedBy = "identifier", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ConferenceRoomEntity> conferenceRooms;


    public OrganizationEntity(String name) {
        this.name = name;
    }


}
