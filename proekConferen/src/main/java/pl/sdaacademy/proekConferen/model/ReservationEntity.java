package pl.sdaacademy.proekConferen.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Data
@Table(name = "reservation")
public class ReservationEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "reservation_id", nullable = false)
    private Integer id;
    private Date startOfBooking;
    private Date endOfBooking;
    private String name;
    private String conferenceRoomBooked;
    private UUID uniqueId;


    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(name = "reserv_room",
            joinColumns = @JoinColumn(name = "reservation_id"),
            inverseJoinColumns = @JoinColumn(name = "conferenceroom_id")
    )
    private List<ConferenceRoomEntity> reservations;


    public ReservationEntity(String name) {
        this.name = name;
    }


}
