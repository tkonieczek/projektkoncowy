package pl.sdaacademy.proekConferen.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "error")
public class ErrorEntity {

    @Id
    @GeneratedValue
    private Integer id;

    private String kod;

    private String message;
    private String requisites;
}
