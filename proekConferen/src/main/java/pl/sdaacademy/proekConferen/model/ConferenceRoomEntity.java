package pl.sdaacademy.proekConferen.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Data
//@NoArgsConstructor
//@RequiredArgsConstructor
//@AllArgsConstructor
@Table(name = "conferenceroom")
public class ConferenceRoomEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "conferenceroom_id", nullable = false)
    private Integer id;
    @NotBlank(message = "Conference room's name is mandatory")
    @Size(min = 2, max = 20)
    @Column(name = "name", nullable = false)
    private String name;
    private String identifier;
    private String levelStatus;
    private Boolean availability;
    private Integer numerOfSeats;

    @ManyToMany(mappedBy = "reservations")
    private List<ReservationEntity> reservations;
// private Set<Author> authors=new HashSet<>();


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id")
    private OrganizationEntity organization;

    @OneToMany(mappedBy = "conferenceRoom", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<EquipmentEntity> equipments;

    public ConferenceRoomEntity(String name, String identifier, Boolean availability, Integer numerOfSeats) {
        this.name = name;
        this.identifier = identifier;
                this.availability = availability;
        this.numerOfSeats = numerOfSeats;
    }
}