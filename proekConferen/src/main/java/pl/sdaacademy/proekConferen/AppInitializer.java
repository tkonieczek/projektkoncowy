package pl.sdaacademy.proekConferen;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.sdaacademy.proekConferen.model.ConferenceRoomEntity;
import pl.sdaacademy.proekConferen.model.OrganizationEntity;
import pl.sdaacademy.proekConferen.repository.ConferenceRoomRepository;
import pl.sdaacademy.proekConferen.repository.OrganizationRepository;

import javax.annotation.PostConstruct;
import javax.persistence.Column;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Component
class AppInitializer {

    private final OrganizationRepository organizationRepository;
    private final ConferenceRoomRepository conferenceRoomRepository;

    @Autowired
    AppInitializer(OrganizationRepository organizationRepository, ConferenceRoomRepository conferenceRoomRepository) {
        this.organizationRepository = organizationRepository;
        this.conferenceRoomRepository = conferenceRoomRepository;
    }


    @PostConstruct
    void init() {
        organizationRepository.save(new OrganizationEntity("Google"));
        organizationRepository.save(new OrganizationEntity("Amazon"));
        organizationRepository.save(new OrganizationEntity("Meta"));

        conferenceRoomRepository.save(new ConferenceRoomEntity("BlueHall", " ", true, 40));
        conferenceRoomRepository.save(new ConferenceRoomEntity("WhiteHall"," ",true,40));
        conferenceRoomRepository.save(new ConferenceRoomEntity("PinkHall","Appel",false,30));

    }
}

